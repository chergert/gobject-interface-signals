#ifndef MY_GADGET_H
#define MY_GADGET_H

#include <glib-object.h>

G_BEGIN_DECLS

#define MY_TYPE_GADGET             (my_gadget_get_type())
#define MY_GADGET(o)               (G_TYPE_CHECK_INSTANCE_CAST((o),    MY_TYPE_GADGET, MyGadget))
#define MY_IS_GADGET(o)            (G_TYPE_CHECK_INSTANCE_TYPE((o),    MY_TYPE_GADGET))
#define MY_GADGET_GET_INTERFACE(o) (G_TYPE_INSTANCE_GET_INTERFACE((o), MY_TYPE_GADGET, MyGadgetInterface))

typedef struct _MyGadget          MyGadget;
typedef struct _MyGadgetInterface MyGadgetInterface;

struct _MyGadgetInterface
{
  GTypeInterface parent;

  void (*frobnicate) (MyGadget *gadget);
};

GType my_gadget_get_type   (void) G_GNUC_CONST;
void  my_gadget_frobnicate (MyGadget *gadget);

G_END_DECLS

#endif /* MY_GADGET_H */
