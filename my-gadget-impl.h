#ifndef MY_GADGET_IMPL_H
#define MY_GADGET_IMPL_H

#include "my-gadget.h"

G_BEGIN_DECLS

#define MY_TYPE_GADGET_IMPL            (my_gadget_impl_get_type())
#define MY_GADGET_IMPL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MY_TYPE_GADGET_IMPL, MyGadgetImpl))
#define MY_GADGET_IMPL_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), MY_TYPE_GADGET_IMPL, MyGadgetImpl const))
#define MY_GADGET_IMPL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  MY_TYPE_GADGET_IMPL, MyGadgetImplClass))
#define MY_IS_GADGET_IMPL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MY_TYPE_GADGET_IMPL))
#define MY_IS_GADGET_IMPL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  MY_TYPE_GADGET_IMPL))
#define MY_GADGET_IMPL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  MY_TYPE_GADGET_IMPL, MyGadgetImplClass))

typedef struct _MyGadgetImpl        MyGadgetImpl;
typedef struct _MyGadgetImplClass   MyGadgetImplClass;
typedef struct _MyGadgetImplPrivate MyGadgetImplPrivate;

struct _MyGadgetImpl
{
  GObject parent;

  /*< private >*/
  MyGadgetImplPrivate *priv;
};

struct _MyGadgetImplClass
{
  GObjectClass parent_class;
};

GType my_gadget_impl_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* MY_GADGET_IMPL_H */
