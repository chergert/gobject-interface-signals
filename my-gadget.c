#include "my-gadget.h"

G_DEFINE_INTERFACE (MyGadget, my_gadget, G_TYPE_OBJECT)

enum {
    FROBNICATE,
    LAST_SIGNAL
};

static guint gSignals [LAST_SIGNAL];

void
my_gadget_frobnicate (MyGadget *gadget)
{
  g_signal_emit (gadget, gSignals [FROBNICATE], 0);
}

static void
my_gadget_default_init (MyGadgetInterface *iface)
{
  gSignals [FROBNICATE] =
    g_signal_new ("frobnicate",
                  MY_TYPE_GADGET,
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (MyGadgetInterface, frobnicate),
                  NULL,
                  NULL,
                  NULL,
                  G_TYPE_NONE,
                  0);
}
