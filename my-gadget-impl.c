#include "my-gadget-impl.h"

static void my_gadget_init (MyGadgetInterface *iface);

G_DEFINE_TYPE_EXTENDED (MyGadgetImpl, my_gadget_impl, G_TYPE_OBJECT, 0,
                        G_IMPLEMENT_INTERFACE (MY_TYPE_GADGET,
                                               my_gadget_init))

static void
my_gadget_impl_class_init (MyGadgetImplClass *klass)
{
}

static void
my_gadget_impl_init (MyGadgetImpl *impl)
{
}

static void
my_gadget_impl_frobnicate (MyGadget *gadget)
{
  g_print ("Hello!\n");
}

static void
my_gadget_init (MyGadgetInterface *iface)
{
  iface->frobnicate = my_gadget_impl_frobnicate;
}
